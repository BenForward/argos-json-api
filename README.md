# README #

### What is this repository for? ###
This repository contains an API and client website to return JSON data from the HUKD API and the Walmart API to compare Argos deals with those of Walmart.

### How do I get set up? ###
To set up, all you need to do is download the git repository and place it on a web server, as long as you can make a "GET" call to the server that is. Once it has been set up, if it is a local machine head to "localhost" and it should be done! If you are not using a local machine, navigate to the URL that was set up with the web server.

### How do I make an API call? ###
To make an API call, you use this format ([BASE_URL] is the URL from the set up):

* [BASE_URL]/api/argos
* [BASE_URL]/api/walmart/[query]

This will then return JSON from both of the APIs used. If these calls don't work, try these:


* [BASE_URL]/index.php/api/argos
* [BASE_URL]/index.php/api/walmart/[query]


### To note: ###
I am not the strongest at building websites, or website UIs (I mainly use Java/C#), but I have completed it so there is no clutter and it is easy to read and follow.

Even though you probably already have seen my LinkedIn profile, there is a link to it at the bottom once the deals have loaded.