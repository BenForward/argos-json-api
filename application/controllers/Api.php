<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Api extends REST_Controller {
    public function argos_get() {
        $xml_url = "http://api.hotukdeals.com/rest_api/v2/?key=e712608bfb73f8850e2c7adc507e25aa&merchant=argos&order=top&forum=deals&results_per_page=12";
        
        if(($xml_data = file_get_contents($xml_url)) == true) {
            $xml = simplexml_load_string($xml_data);
            
            if(!$xml) {
                $json = ['error' => 'Could not open the XML URL'];    
                $this->response($json, 400);
            } else {
                $this->response($xml, 200);
            }
        } else {
            $json = $json = ['error' => 'Could not load the XML URL'];  
            $this->response($json, 400);
        }
    }
    
    public function walmart_get($query) {
        $url = "http://api.walmartlabs.com/v1/search?apiKey=4pw8dfnrp33a4ww3uaqyxgue&query=".$query."&numItems=1";
        
        if(($json = file_get_contents($url)) == true) {   
            $json = json_decode($json, true);
            
            $price = $json['items'][0]['salePrice'];
            
            //Exchange rate as of 07/02/2016
            $price = round($price * 0.69, 2);
            $json['items'][0]['salePrice'] = $price;
            
            $json['items'][0]['standardShipRate'] = 20.00;
    
            $this->response($json, 200);
        } else {
            $json = $json = ['error' => 'Could not find a matching product'];  
            $this->response($json, 400);
        }
    }
}
