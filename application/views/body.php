        <script type="text/javascript">
            function loadDeals() {                
                $.get("http://argosapi:SuperSecurePass123!@localhost/index.php/api/argos", function(argos) {
                    for(i = 0; i < 12; i++) {
                        $items = argos['deals']['api_item'][i];
                        $title = $items['title'];
                        $pic = $items['deal_image'];
                        $arg_price = $items['price'];
                        $id = $pic.match("http://static.hotukdeals.com/images/threads/(.*).jpg");
                        
                        $title = $title.replace("+", "");
                        $title = $title.replace(" ", "+");
                        $title = $title.replace("£", "");
                        $title = $title.replace("(", "");
                        $title = $title.replace(")", "");
                        $title = $title.replace("@", "");
                        $title = $title.replace("!", "");
                        $title = $title.replace(".", "");
                        $title = $title.replace("-", "");
                        $title = $title.replace("/", "");
                        $title = $title.replace("\\", "");
                        $title = $title.replace("!", "");
                        
                        $.ajax({
                            url: "http://argosapi:SuperSecurePass123!@localhost/index.php/api/walmart/" + $title,
                            type: "GET",
                            async: false,
                            success: function(walmart) {
                                $wal = walmart['items'][0];
                                $wal_price = Math.round($wal['salePrice'] * 100) / 100;;
                                $difference = Math.round(($wal_price - $arg_price) * 100) / 100;
                                
                                $(".item" +i).children(".title").text($items['title']);
                                $(".item" +i).children(".image").attr("src", $pic);
                                $(".item" +i).children(".desc").text($items['description']);
                                $(".item" +i).children(".pricep").children(".price").text($arg_price);
                                $(".item" +i).children(".tempp").children(".temp").text($items['temperature']);
                                $(".item" +i).children(".dealp").children(".deal").attr("href", $items['deal_link']);
                                $(".item" +i).children(".argosp").children(".argos").attr("href", "http://www.hotukdeals.com/visit?m=5&q=" + $id[1]);
                                $(".item" +i).children(".walp").children(".walmart").text($wal_price);
                                $(".item" +i).children(".walp").children(".difference").text($difference);
                                
                                if(i == 11) 
                                    $(".loading").fadeOut("slow");
                                    $(".deals").fadeIn("slow");
                                    $(".footer").html("&copy; | <a href=\"https://uk.linkedin.com/in/benforward\">Ben Forward</a> | <?php echo date("Y"); ?>");
                            }
                        });
                    }
                }); 
            }
            
            function updateItems($items, $walmart, $int) {
                
                
                
                
            }
        </script>

        <div id="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h1>Welcome to the top Argos deals on HUKD</h1>
                </div>
            </div>
    
            <br>
    
            <div class="row loading">
                <div class="col-md-12">
                    <h1>Loading deals...</h1>
                </div>
            </div>
    
            <br> <br>
    
            <div class="deals">
                <div class="row">
                    <div class="col-md-2 col-md-offset-1 item-container item0">
                        <h4 class="title">Title</h4>
                        <img class="image" src="#" width="250" height="250">
                        <p class="desc">Generic description</p>
                        <p class="pricep">&pound;<span class="price">10</span></p>
                        <p class="tempp"><span class="temp">100</span>&deg;C</p>
                        <p class="dealp">Click <a href="#" class="deal" target="_blank">here</a> to view the deal.</p>
                        <p class="argosp">Click <a href="#" class="argos" target="_blank">here</a> to view it at Argos</p>
                        <p class="walp">*Walmart price: &pound;<span class="walmart">100</span>. Difference: &pound;<span class="difference">90</span></p>
                    </div>
            
                    <div class="col-md-2 col-md-offset-2 item-container item1">
                        <h4 class="title">Title</h4>
                        <img class="image" src="#" width="250" height="250">
                        <p class="desc">Generic description</p>
                        <p class="pricep">&pound;<span class="price">10</span></p>
                        <p class="tempp"><span class="temp">100</span>&deg;C</p>
                        <p class="dealp">Click <a href="#" class="deal" target="_blank">here</a> to view the deal.</p>
                        <p class="argosp">Click <a href="#" class="argos" target="_blank">here</a> to view it at Argos</p>
                        <p class="walp">*Walmart price: &pound;<span class="walmart">100</span>. Difference: &pound;<span class="difference">90</span></p>
                    </div>
        
                    <div class="col-md-2 col-md-offset-2 item-container item2">
                        <h4 class="title">Title</h4>
                        <img class="image" src="#" width="250" height="250">
                        <p class="desc">Generic description</p>
                        <p class="pricep">&pound;<span class="price">10</span></p>
                        <p class="tempp"><span class="temp">100</span>&deg;C</p>
                        <p class="dealp">Click <a href="#" class="deal" target="_blank">here</a> to view the deal.</p>
                        <p class="argosp">Click <a href="#" class="argos" target="_blank">here</a> to view it at Argos</p>
                        <p class="walp">*Walmart price: &pound;<span class="walmart">100</span>. Difference: &pound;<span class="difference">90</span></p>
                    </div>
                </div>

                <br> <br>

                <div class="row">
                    <div class="col-md-2 col-md-offset-1 item-container item3">
                        <h4 class="title">Title</h4>
                        <img class="image" src="#" width="250" height="250">
                        <p class="desc">Generic description</p>
                        <p class="pricep">&pound;<span class="price">10</span></p>
                        <p class="tempp"><span class="temp">100</span>&deg;C</p>
                        <p class="dealp">Click <a href="#" class="deal" target="_blank">here</a> to view the deal.</p>
                        <p class="argosp">Click <a href="#" class="argos" target="_blank">here</a> to view it at Argos</p>
                        <p class="walp">*Walmart price: &pound;<span class="walmart">100</span>. Difference: &pound;<span class="difference">90</span></p>
                    </div>
            
                    <div class="col-md-2 col-md-offset-2 item-container item4">
                        <h4 class="title">Title</h4>
                        <img class="image" src="#" width="250" height="250">
                        <p class="desc">Generic description</p>
                        <p class="pricep">&pound;<span class="price">10</span></p>
                        <p class="tempp"><span class="temp">100</span>&deg;C</p>
                        <p class="dealp">Click <a href="#" class="deal" target="_blank">here</a> to view the deal.</p>
                        <p class="argosp">Click <a href="#" class="argos" target="_blank">here</a> to view it at Argos</p>
                        <p class="walp">*Walmart price: &pound;<span class="walmart">100</span>. Difference: &pound;<span class="difference">90</span></p>
                    </div>
        
                    <div class="col-md-2 col-md-offset-2 item-container item5">
                        <h4 class="title">Title</h4>
                        <img class="image" src="#" width="250" height="250">
                        <p class="desc">Generic description</p>
                        <p class="pricep">&pound;<span class="price">10</span></p>
                        <p class="tempp"><span class="temp">100</span>&deg;C</p>
                        <p class="dealp">Click <a href="#" class="deal" target="_blank">here</a> to view the deal.</p>
                        <p class="argosp">Click <a href="#" class="argos" target="_blank">here</a> to view it at Argos</p>
                        <p class="walp">*Walmart price: &pound;<span class="walmart">100</span>. Difference: &pound;<span class="difference">90</span></p>
                    </div>
                </div>

                <br> <br>

                <div class="row">
                    <div class="col-md-2 col-md-offset-1 item-container item6">
                        <h4 class="title">Title</h4>
                        <img class="image" src="#" width="250" height="250">
                        <p class="desc">Generic description</p>
                        <p class="pricep">&pound;<span class="price">10</span></p>
                        <p class="tempp"><span class="temp">100</span>&deg;C</p>
                        <p class="dealp">Click <a href="#" class="deal" target="_blank">here</a> to view the deal.</p>
                        <p class="argosp">Click <a href="#" class="argos" target="_blank">here</a> to view it at Argos</p>
                        <p class="walp">*Walmart price: &pound;<span class="walmart">100</span>. Difference: &pound;<span class="difference">90</span></p>
                    </div>
            
                    <div class="col-md-2 col-md-offset-2 item-container item7">
                        <h4 class="title">Title</h4>
                        <img class="image" src="#" width="250" height="250">
                        <p class="desc">Generic description</p>
                        <p class="pricep">&pound;<span class="price">10</span></p>
                        <p class="tempp"><span class="temp">100</span>&deg;C</p>
                        <p class="dealp">Click <a href="#" class="deal" target="_blank">here</a> to view the deal.</p>
                        <p class="argosp">Click <a href="#" class="argos" target="_blank">here</a> to view it at Argos</p>
                        <p class="walp">*Walmart price: &pound;<span class="walmart">100</span>. Difference: &pound;<span class="difference">90</span></p>
                    </div>
        
                    <div class="col-md-2 col-md-offset-2 item-container item8">
                        <h4 class="title">Title</h4>
                        <img class="image" src="#" width="250" height="250">
                        <p class="desc">Generic description</p>
                        <p class="pricep">&pound;<span class="price">10</span></p>
                        <p class="tempp"><span class="temp">100</span>&deg;C</p>
                        <p class="dealp">Click <a href="#" class="deal" target="_blank">here</a> to view the deal.</p>
                        <p class="argosp">Click <a href="#" class="argos" target="_blank">here</a> to view it at Argos</p>
                        <p class="walp">*Walmart price: &pound;<span class="walmart">100</span>. Difference: &pound;<span class="difference">90</span></p>
                    </div>
                </div>

                <br> <br>

                <div class="row">
                    <div class="col-md-2 col-md-offset-1 item-container item9">
                        <h4 class="title">Title</h4>
                        <img class="image" src="#" width="250" height="250">
                        <p class="desc">Generic description</p>
                        <p class="pricep">&pound;<span class="price">10</span></p>
                        <p class="tempp"><span class="temp">100</span>&deg;C</p>
                        <p class="dealp">Click <a href="#" class="deal" target="_blank">here</a> to view the deal.</p>
                        <p class="argosp">Click <a href="#" class="argos" target="_blank">here</a> to view it at Argos</p>
                        <p class="walp">*Walmart price: &pound;<span class="walmart">100</span>. Difference: &pound;<span class="difference">90</span></p>
                    </div>
            
                    <div class="col-md-2 col-md-offset-2 item-container item10">
                        <h4 class="title">Title</h4>
                        <img class="image" src="#" width="250" height="250">
                        <p class="desc">Generic description</p>
                        <p class="pricep">&pound;<span class="price">10</span></p>
                        <p class="tempp"><span class="temp">100</span>&deg;C</p>
                        <p class="dealp">Click <a href="#" class="deal" target="_blank">here</a> to view the deal.</p>
                        <p class="argosp">Click <a href="#" class="argos" target="_blank">here</a> to view it at Argos</p>
                        <p class="walp">*Walmart price: &pound;<span class="walmart">100</span>. Difference: &pound;<span class="difference">90</span></p>
                    </div>
        
                    <div class="col-md-2 col-md-offset-2 item-container item11">
                        <h4 class="title">Title</h4>
                        <img class="image" src="#" width="250" height="250">
                        <p class="desc">Generic description</p>
                        <p class="pricep">&pound;<span class="price">10</span></p>
                        <p class="tempp"><span class="temp">100</span>&deg;C</p>
                        <p class="dealp">Click <a href="#" class="deal" target="_blank">here</a> to view the deal.</p>
                        <p class="argosp">Click <a href="#" class="argos" target="_blank">here</a> to view it at Argos</p>
                        <p class="walp">*Walmart price: &pound;<span class="walmart">100</span>. Difference: &pound;<span class="difference">90</span></p>
                    </div>
                </div>
                
                <div class="col-md-2 col-md-offset-1">
                    <p>*This is the closes Walmart product on the market converted into GBP and with a £20 delivery fee added.</p>
                </div>
                
                <br> <br>
            </div>
        </div>