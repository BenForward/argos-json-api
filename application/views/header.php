<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" context="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <meta name="description" content="The top Argos deals on HUKD compaired to the most relevant item at Walmart.">

        <title>Top Argos Deals</title>

        <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
      
        <style>
            
            h1 {
                text-align: center; 
            }
            
            .row {
                text-align: center; 
            }
            
            .item-container {
                border: 5px solid grey;
                border-radius: 45px;
                padding: 15px 15px 15px 15px;
                min-width: 300px;
            }
            
            .deals {
                display: none;    
            }
            
            .loading {
                color: blue;
            }
        </style>
    </head>
    
    <body onload="loadDeals()">